
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('catalog_states').del()
    .then(function () {
      // Inserts seed entries
      return knex('catalog_states').insert([
        {name: 'uno'},
        {name: 'dos'},
        {name: 'tres'},
      ]);
    });
};
