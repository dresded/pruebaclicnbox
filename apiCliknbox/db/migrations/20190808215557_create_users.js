exports.up = function (knex) {
    return knex.schema.createTableIfNotExists('catalog_states', function (table) {
      table.increments()
      table.text('name')
      table.boolean('status').defaultTo(true)
    })
  }

  exports.down = function (knex) {
    return knex.schema.dropTable('catalog_states')
  }
