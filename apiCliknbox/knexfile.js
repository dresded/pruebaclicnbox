module.exports = {
  development: {
    client: 'pg',
    connection: 'postgres://postgres:facil@localhost:5432/postgres',
    migrations: {
      directory: __dirname + '/db/migrations',
    },
    seeds: {
      directory: __dirname + '/db/seeds',
    }
  },
  test: {
    client: 'pg',
    connection: 'postgres://postgres:facil@localhost:5432/postgres'
  }
}