const knex = require('../db/knex')

module.exports = {
    async getPromedioUsuario () {
        const fields = [
            'usuarios.nombre',
            knex.raw('round(avg(calificacion)) AS promedio')
        ]
        const query = knex.select(fields)
        .from('tareas_proyectos')
        .innerJoin('usuarios', 'usuarios.id', 'encargado')
        .innerJoin('entregas_por_tareas', 'entregas_por_tareas.id_tarea', 'tareas_proyectos.id')
        .groupBy('usuarios.nombre')

        return query
    },

    async calificacionProyectos () {
        const fields = [
            'proyecto',
            knex.raw('min(calificacion) AS calificacion_minima'),
            knex.raw('max(calificacion) AS calificacion_maxima')
        ]
        const query = knex.select(fields)
        .from('tareas_proyectos')
        .innerJoin('entregas_por_tareas', 'entregas_por_tareas.id_tarea', 'tareas_proyectos.id')
        .groupBy('proyecto')

        return query
    }
}