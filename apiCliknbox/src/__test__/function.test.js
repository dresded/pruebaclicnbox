// import functions from '../functions'
var functions = require('../functions');

const averageByUsers = [
    {
        "nombre": "edwin",
        "promedio": "50"
    },
    {
        "nombre": "vicente",
        "promedio": "55"
    }
]

const groupOfProjects = [
    {
        "proyecto": 1,
        "calificacion_minima": 30,
        "calificacion_maxima": 70
    },
    {
        "proyecto": 2,
        "calificacion_minima": 60,
        "calificacion_maxima": 80
    },
    {
        "proyecto": 3,
        "calificacion_minima": 30,
        "calificacion_maxima": 50
    }
]

describe('get average user tasks', () => {
    it('should get the list of users with the average of their tasks', () => {
        return functions.getPromedioUsuario()
            .then(result => {
            expect(result).toEqual(averageByUsers)
        })
    })
})

describe('group projects by type and give the minimum and maximum of the qualifications', () => {
    it('should get the list of group projects by type', () => {
        return functions.calificacionProyectos()
            .then(result => {
            expect(result).toEqual(groupOfProjects)
        })
    })
})