var express = require('express');
var bodyParser = require('body-parser');
var port = process.env.PORT || 8001;
var knex = require('./db/knex');
var functions = require('./src/functions');

var app = express();

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }));

app.get('/promedio-usuarios', function(req, res) {
    functions.getPromedioUsuario()
    .then((query) => res.send(query))
})

app.get('/calificacion_proyecto', function(req, res) {
    functions.calificacionProyectos()
    .then((query) => res.send(query))
})

app.listen(port, function() {
    console.log("listening on port: ", port)
})