let js1 = {}
let js2 = { foo: { bar: 1 }, fooo: { bar: 4, foo: { foo: 1 } } }
let js3 = { foo: [{ bar: 1 }], bar: { bar: 4, foo: { foo: 1 } } }
let js4 = { foo: 'lol' }

const foldJSON = (obj) => {
    const num = Object.keys(obj).reduce((acumm, elemnt) => {
        if (typeof obj[elemnt] === 'object' && !Array.isArray(obj[elemnt])) {
            if (elemnt !== 'bar') acumm++
        } else {
            throw new Error("Error!");
        }
        return acumm
    }, 0)
    return {"result": num}
}

try {
    js1 = foldJSON(js1)
    console.log('****************************************************************')
    console.log('foldJSON1', js1)
    console.log('foldJSON1 js1.result === 0; // should be true ->', js1.result === 0)
    console.log('****************************************************************')
} catch (error) {
    console.log(error.message)
}

try {
    js2 = foldJSON(js2)
    console.log('****************************************************************')
    console.log('foldJSON2', js2)
    console.log('foldJSON2 js2.result === 2; // should be true ->', js2.result === 2)
    console.log('****************************************************************')
} catch (error) {
    console.log(error.message)
}

try {
    console.log('foldJSON3', foldJSON(js3))
} catch (error) {
    console.log(error.message)
}

try {
    console.log('foldJSON4', foldJSON(js4))
} catch (error) {
    console.log(error.message)
}
