const str = 'abcdefg'

const reverse = (string) => {
  let newString = ''
  for (i = string.length - 1; i >= 0; i--) {
    newString = newString + string[i]
  }
  return newString
}

console.log('reverse->', reverse(str))
console.log(`reverse == "gfedcba" // should be true --> `, reverse(str) === 'gfedcba')