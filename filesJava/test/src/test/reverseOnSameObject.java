package test;

public class reverseOnSameObject {
	static String str = "abcdef";
	public static void main(String[] args) {
		String newStr = reverse(str);
		System.out.println("newStr-> " + newStr);
		System.out.println("str !==\"abcd\"; // should be true -> " + (newStr.equals("fedcba")));
	}
	
	public static String reverse (String str) {
		String strArray[] = str.split("");
		str = "";
		for(int i=0; i < strArray.length; i++){
			str = strArray[i] + str;
		}
		return str;
	}
}
