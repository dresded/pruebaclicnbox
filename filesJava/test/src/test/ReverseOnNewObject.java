package test;

public class ReverseOnNewObject {

	public static void main(String[] args) {
		String str = "abcdef";
		String newStr = reverse(str);
		System.out.println("newStr-> " + newStr);
		System.out.println("rev ==='fedcba' should be true -> " + (newStr.equals("fedcba")));
	}
	
	public static String reverse (String str) {
		String strArray[] = str.split("");
		String strReverse = "";
		for(int i=0; i < strArray.length; i++){
			strReverse = strArray[i] + strReverse;
		}
		
		return strReverse;
	}
}